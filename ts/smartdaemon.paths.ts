import * as plugins from './smartdaemon.plugins.js';

export const packageDir = plugins.path.join(plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url), '../');
export const systemdDir = plugins.path.join('/lib/systemd/system/');
