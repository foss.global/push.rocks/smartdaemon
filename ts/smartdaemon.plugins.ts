// node native scope
import * as path from 'path';

export { path };

// @pushrocks scope
import * as lik from '@pushrocks/lik';
import * as smartfile from '@pushrocks/smartfile';
import * as smartfm from '@pushrocks/smartfm';
import * as smartlog from '@pushrocks/smartlog';
import * as smartlogDestinationLocal from '@pushrocks/smartlog-destination-local';
import * as smartpath from '@pushrocks/smartpath';
import * as smartshell from '@pushrocks/smartshell';
import * as smartsystem from '@pushrocks/smartsystem';

export { lik, smartfile, smartfm, smartlog, smartlogDestinationLocal, smartpath, smartshell, smartsystem };

// third party
