/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartdaemon',
  version: '2.0.3',
  description: 'start scripts as long running daemons and manage them'
}
