import { expect, tap } from '@pushrocks/tapbundle';

import * as plugins from './plugins.js';

import * as smartdaemon from '../ts/index.js';



let testSmartdaemon: smartdaemon.SmartDaemon;

tap.test('should create an instance of smartdaemon', async () => {
  testSmartdaemon = new smartdaemon.SmartDaemon();
  expect(testSmartdaemon).toBeInstanceOf(smartdaemon.SmartDaemon);
});

tap.test('should create a service', async () => {
  testSmartdaemon.addService({
    name: 'npmversion',
    version: 'x.x.x',
    command: 'npm -v',
    description: 'displays the npm version',
    workingDir: plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
  });
});

tap.start();
